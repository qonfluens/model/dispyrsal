from setuptools import setup

setup(
    name='dispyrsal',
    version='1.0.0',
    description='dispersal + python',
    packages=["dispyrsal"],
    install_requires=["numpy","scipy","matplotlib"],
)

