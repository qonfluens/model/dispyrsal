import numpy as np

def moore(r:int):
    s=r*2+1
    m = np.ones((s,s))
    return m

def neuman(r:int):
    s=r*2+1
    m = np.ones((s,s))
    for i in range(0,r):
        m[i,(r+i+1):]=0
    for i in range(0,r):
        m[(r+i+1):,i]=0
    for i in range(0,r):
        m[i,:(r-i)]=0
    for i in range(0,r):
        m[r+i+1,(2*r-i):]=0
    return m

def distrib_kernel(kernel,v):
    r = int((len(kernel)-1)/2)
    for i in range(0,len(v)-1):
        kernel[r-i:r+i,r-i] = v[i]
        kernel[r-i:r+i,r+i] = v[i]
        kernel[r-i,r-i:r+i] = v[i]
        kernel[r+i,r-i:r+i] = v[i]
    return kernel / np.sum(kernel)
    