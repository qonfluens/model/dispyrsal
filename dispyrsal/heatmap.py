import matplotlib.pyplot as plt
import numpy as np

# Pour faire les heatmap rapidement
def heatmap2d(arr: np.ndarray):
    plt.imshow(arr, cmap='viridis')
    plt.colorbar()
    plt.show()